package modele;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;




public class BaseD {
	
	
	//variables 
	private  String  bdd , serveur , user, mdp;  
	private Connection maconexion; 
	
	
	
	public BaseD(String serveur, String bdd, String user, String mdp) {
		
		try {
			
				this.serveur = serveur;
				this.bdd = bdd;
				this.user = user;
				this.mdp= mdp; 
				this.maconexion = null;
		
		}catch(Exception exp) {
			
			System.out.println("Erreur de Conexcion Base données " + exp);
			
		}
		
		
		
	}
	
	
	
	
	// futionnes .. 
	
	public void chargerPilote() {
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException exp) {
			
			System.out.println("Erreur de chargement du pilote");
			
		}
	}

	
	
	
	public void seConnecter() {
		
		String url = "mysql://" + this.serveur + "/" + this.bdd;
		
		try {
			
			this.maconexion = DriverManager.getConnection("jdbc:" + url, this.user, this.mdp);
			
			
		} catch (SQLException exp) {
			
			System.out.println("Erreur de connexion au : " + url);
			
		}
		
	}
	
	
	
	
	public void seDeconnecter() {
		
		try {
			
			if (this.maconexion != null) {
				this.maconexion.close();
			}
			
		} catch (SQLException exp) {
			
			System.out.println("Erreur de fermeture de la connexion !");
			
		}
		
	}
	
	
	
	public Connection getMaconnexion() {
		
		return this.maconexion;
		
	}
	
	
	
	
	
	

}
