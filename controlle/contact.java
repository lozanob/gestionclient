package controlle;

public class contact {
	
	
	private int Id , Tel;
	private String Nom, Prenom ; 

	
	
	public contact( String nom , String prenom , int tel , int id) {
		this.Nom=nom;
		this.Prenom=prenom;
		this.Tel=tel; 
		this.Id= id; 
	}
	

	
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + ((Nom == null) ? 0 : Nom.hashCode());
		result = prime * result + ((Prenom == null) ? 0 : Prenom.hashCode());
		result = prime * result + Tel;
		return result;
	}






	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		contact other = (contact) obj;
		if (Id != other.Id)
			return false;
		if (Nom == null) {
			if (other.Nom != null)
				return false;
		} else if (!Nom.equals(other.Nom))
			return false;
		if (Prenom == null) {
			if (other.Prenom != null)
				return false;
		} else if (!Prenom.equals(other.Prenom))
			return false;
		if (Tel != other.Tel)
			return false;
		return true;
	}






	public String toString() {
		
		return this.Id + " - " + this.Nom + " - " + this.Prenom + " - " + this.Tel;
	}



	public int getId() {
		return Id;
	}



	public void setId(int id) {
		Id = id;
	}




	public int getTel() {
		return Tel;
	}





	public void setTel(int tel) {
		Tel = tel;
	}




	public String getNom() {
		return Nom;
	}





	public void setNom(String nom) {
		Nom = nom;
	}







	public String getPrenom() {
		return Prenom;
	}







	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	

}
