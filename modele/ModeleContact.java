package modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import controlle.contact;

public class ModeleContact {

	
	public static LinkedList<contact> selectAll() {

		LinkedList<contact> uneListe = new LinkedList<contact>();
		BaseD uneBDD = new BaseD("localhost", "gestionClient", "root", "");
		uneBDD.chargerPilote();
		uneBDD.seConnecter();
		String requete = "Select * from contact;";
		
		
		try {

			Statement unStat = uneBDD.getMaconnexion().createStatement();
			ResultSet unRes = unStat.executeQuery(requete);
			
			while (unRes.next()) {
				
				int id = unRes.getInt("Id_contact");
				String nom = unRes.getString("Nom_contact");
				String prenom = unRes.getString("Prenom_contact");
				int Tel = unRes.getInt("Tel_contact");
				
				contact unContact = new contact(nom, prenom, Tel,id);
				uneListe.add(unContact);

			}
			
			unStat.close();
			unRes.close();

		} catch (SQLException exp) {
			System.out.println("Erreur d'execution :" + requete);
		}

		uneBDD.seDeconnecter();
		return uneListe;
	}

	
	public static void insertContact(contact unContact) {
		
	
		String requete = "insert into unContact(Nom_contact, Prenom_contact,Tel_contact) values ('"+ unContact.getNom() + "','" + unContact.getTel()+ "');";
		BaseD uneBDD = new BaseD("localhost", "gestionClient", "root", "");
		uneBDD.chargerPilote();
		uneBDD.seConnecter();
		
		try {
			
			Statement unStat = uneBDD.getMaconnexion().createStatement();
			unStat.execute(requete);
			unStat.close();
			
		} catch (SQLException exp) {
			
			System.out.println("Erreur de la requete : " + requete);
		}
		
		uneBDD.seDeconnecter();
	}

	
	
	
	public static LinkedList<contact> selectWhere(String cle) {
		
		LinkedList<contact> uneListe = new LinkedList<contact>();
		BaseD uneBDD = new BaseD("localhost", "gestionClient", "root", "");
		uneBDD.chargerPilote();
		uneBDD.seConnecter();
		
		String requete = "select * from contact where Nom_contact like '%" + cle + "%'" + " or Prenom_contact like '%" + cle
				+ "%'" + " or Tel_contact like '%" + cle + "%'";
		
		try {
			Statement unStat = uneBDD.getMaconnexion().createStatement();
			ResultSet unRes = unStat.executeQuery(requete);
			
			while (unRes.next()) {
				
				String nom = unRes.getString("Nom_contact");
				String prenom = unRes.getString("Prenom_contact");
				int tel = unRes.getInt("Tel_contact");
				int id = unRes.getInt("Id_contact");
				

				contact unContact = new contact(nom, prenom, tel, id );
				uneListe.add(unContact);

			}
			
			unStat.close();
			unRes.close();
			
		} catch (SQLException exp) {
			
			System.out.println("Erreur d'execution :" + requete);
		}

		uneBDD.seDeconnecter();
		return uneListe;
	}

	
	public static int deleteContact(String cle) {
		
		BaseD uneBDD = new BaseD("localhost", "gestionClient", "root", "");
		uneBDD.chargerPilote();
		uneBDD.seConnecter();

		int nb = 0;
		String requete1 = "select count(*) from contact where Nom_contact like '%" + cle + "%' OR Prenom_contact like '%"
				+ cle + "%' OR Tel_contact like '%" + cle  + "%' OR Id_contact like '%" + cle + "%';";
		
		String requete2 = "delete from contact where Nom_contact like '%" + cle + "%' OR Prenom_contact like '%"
				+ cle + "%' OR Tel_contact like '%" + cle  + "%' OR Id_contact like '%" + cle + "%';";

		try {
			Statement unStat = uneBDD.getMaconnexion().createStatement();
			ResultSet unRes = unStat.executeQuery(requete1);
			unRes.next();
			nb = unRes.getInt(1);
			if (nb > 0) {
				unStat.execute(requete2);
			}
			unStat.close();
			unRes.close();
		}

		catch (SQLException exp) {
			System.out.println("Erreur d'exécution : " + requete1 + " ou " + requete2);
		}

		uneBDD.seDeconnecter();

		return nb;

	}

	
	
	public static void updateContact(contact unContact) {

		BaseD uneBDD = new BaseD("localhost", "gestionClient", "root", "");
		uneBDD.chargerPilote();
		uneBDD.seConnecter();

		int nb = 0;
		String requete = "update contact set " + "Nom_contact='" + unContact.getNom() + "'," + 
						"Prenom_contact='"+ unContact.getPrenom() + "'," +
						"Tel_contact='" + unContact.getTel() + "'" + 
						"where reference='"+ unContact.getId() + "';";

		try {
			Statement unStat = uneBDD.getMaconnexion().createStatement();
			unStat.execute(requete);
			unStat.close();

		}

		catch (SQLException exp) {
			System.out.println("Erreur d'exécution : " + requete);
		}

		uneBDD.seDeconnecter();

	}
}
